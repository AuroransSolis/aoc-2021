use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<u32> {
    input
        .split_ascii_whitespace()
        .map(|line| {
            match line.as_bytes() {
                &[b0, b1, b2] => u32::from_le_bytes([b2, b1, b0, 0]),
                &[b0, b1, b2, b3] => u32::from_le_bytes([b3, b2, b1, b0]),
                _ => unreachable!(),
            }
        })
        .collect::<_>()
}

#[aoc(day1, part1)]
pub fn part1(input: &[u32]) -> usize {
    input
        .windows(2)
        .filter(|window| window[1] > window[0])
        .count()
}

#[aoc(day1, part2)]
pub fn part2(input: &[u32]) -> usize {
    input
        .windows(4)
        .filter(|window| window[3] > window[0])
        .count()
}
