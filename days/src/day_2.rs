use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day2)]
pub fn input_generator(input: &str) -> Vec<[isize; 2]> {
    input
        .lines()
        .map(|line| {
            let (dir, amt) = line.split_at(line.len() - 1);
            let amt = amt.parse::<isize>().unwrap();
            match dir {
                "forward " => [amt, 0],
                "down " => [0, amt],
                "up " => [0, -amt],
                _ => panic!(),
            }
        })
        .collect::<_>()
}

#[aoc(day2, part1)]
pub fn part1(input: &[[isize; 2]]) -> isize {
    let [x, y] = input
        .iter()
        .copied()
        .reduce(|[x, y], [dx, dy]| [x + dx, y + dy])
        .unwrap();
    x * y
}

#[aoc(day2, part1, onlyiter)]
pub fn part1_onlyiter(input: &[[isize; 2]]) -> isize {
    input
        .iter()
        .copied()
        .reduce(|[x, y], [dx, dy]| [x + dx, y + dy])
        .unwrap()
        .into_iter()
        .product()
}

#[aoc(day2, part2)]
pub fn part2(input: &[[isize; 2]]) -> isize {
    let [x, y, _] = input
        .iter()
        .copied()
        .fold([0; 3], |[x, y, aim_units], [forward, d_aim]| {
            [x + forward, y + forward * aim_units, aim_units + d_aim]
        });
    x * y
}

#[aoc(day2, part2, onlyiter)]
pub fn part2_onlyiter(input: &[[isize; 2]]) -> isize {
    input
        .iter()
        .copied()
        .fold([0; 3], |[x, y, aim_units], [forward, d_aim]| {
            [x + forward, y + forward * aim_units, aim_units + d_aim]
        })
        .into_iter()
        .product()
}
