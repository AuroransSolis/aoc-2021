// This one might be SIMD-able?

use aoc_runner_derive::aoc;

#[inline(always)]
fn count_bits_into(line: &str, counts: &mut [isize]) {
    line
        .bytes()
        .zip(counts.iter_mut())
        .for_each(|(bit, count)| {
            if bit == b'0' {
                *count -= 1;
            } else {
                *count += 1;
            }
        });
}

#[inline(always)]
fn get_bit_counts(input: &str) -> Vec<isize> {
    let mut lines = input.lines();
    let first = lines.next().unwrap();
    let mut counts: Vec<isize> = Vec::with_capacity(first.len());
    for _ in 0..counts.capacity() {
        counts.push(0);
    }
    count_bits_into(first, &mut counts);
    lines.for_each(|line| count_bits_into(line, &mut counts));
    counts
}

#[inline(always)]
fn get_gamma_and_epsilon(input: &str) -> [usize; 2] {
    let counts = get_bit_counts(input);
    let mask = !(!0 << counts.len());
    let gamma = counts
        .into_iter()
        .rev()
        .enumerate()
        .map(|(pos, bit)| ((bit > 0) as usize) << pos)
        .sum::<usize>();
    [gamma, !gamma & mask]
}

#[aoc(day3, part1)]
pub fn part1(input: &str) -> usize {
    let [gamma, epsilon] = get_gamma_and_epsilon(input);
    gamma * epsilon
}

#[inline(always)]
fn bin_string_to_usize(string: &str) -> usize {
    string
        .as_bytes()
        .iter()
        .rev()
        .enumerate()
        .map(|(pos, &byte)| ((byte - b'0') as usize) << pos)
        .sum::<usize>()
}

#[inline(always)]
fn bit_count_at(nums: &[usize], bit: usize) -> isize {
    nums
        .iter()
        .map(|&num| {
            if num & (1 << bit) > 0 {
                1isize
            } else {
                -1
            }
        })
        .sum::<isize>()
}

#[aoc(day3, part2)]
pub fn part2(input: &str) -> usize {
    let mut lines = input.lines();
    let first = lines.next().unwrap();
    let bits = first.len();
    let first = bin_string_to_usize(first);
    let mut nums = input
        .lines()
        .map(bin_string_to_usize)
        .collect::<Vec<_>>();
    nums.push(first);
    let mut comms = nums.clone();
    let mut uncomms = nums;
    for bit in (0..bits).rev() {
        if comms.len() <= 1 && uncomms.len() <= 1 {
            break;
        }
        if comms.len() > 1 {
            let count = bit_count_at(&comms, bit);
            let comm_bit = if count >= 0 {
                1 << bit
            } else {
                0
            };
            let check_bit = |num: usize| num & (1 << bit) == comm_bit;
            let mut ind = 0;
            while ind < comms.len() {
                if check_bit(comms[ind]) {
                    ind += 1;
                } else {
                    let _ = comms.swap_remove(ind);
                }
            }
        }
        if uncomms.len() > 1 {
            let count = bit_count_at(&uncomms, bit);
            let uncomm_bit = if count >= 0 {
                0
            } else {
                1 << bit
            };
            let check_bit = |num: usize| num & (1 << bit) == uncomm_bit;
            let mut ind = 0;
            while ind < uncomms.len() {
                if check_bit(uncomms[ind]) {
                    ind += 1;
                } else {
                    let _ = uncomms.swap_remove(ind);
                }
            }
        }
    }
    comms[0] * uncomms[0]
}

fn check_one_at(num: usize, bit: usize) -> bool {
    num & (1 << bit) > 0
}

fn check_zero_at(num: usize, bit: usize) -> bool {
    num & (1 << bit) == 0
}

#[aoc(day3, part2, optusize)]
pub fn part2_optusize(input: &str) -> usize {
    let mut lines = input.lines();
    let first = lines.next().unwrap();
    let bits = first.len();
    let first = bin_string_to_usize(first);
    let mut nums = input
        .lines()
        .map(bin_string_to_usize)
        .collect::<Vec<_>>();
    nums.push(first);
    let mut comms = nums.clone();
    let mut uncomms = nums;
    for bit in (0..bits).rev() {
        if comms.len() <= 1 && uncomms.len() <= 1 {
            break;
        }
        if comms.len() > 1 {
            let count = bit_count_at(&comms, bit);
            let check_bit = if count >= 0 {
                check_one_at
            } else {
                check_zero_at
            };
            let mut ind = 0;
            while ind < comms.len() {
                if check_bit(comms[ind], bit) {
                    ind += 1;
                } else {
                    let _ = comms.swap_remove(ind);
                }
            }
        }
        if uncomms.len() > 1 {
            let count = bit_count_at(&uncomms, bit);
            let check_bit = if count >= 0 {
                check_zero_at
            } else {
                check_one_at
            };
            let mut ind = 0;
            while ind < uncomms.len() {
                if check_bit(uncomms[ind], bit) {
                    ind += 1;
                } else {
                    let _ = uncomms.swap_remove(ind);
                }
            }
        }
    }
    comms[0] * uncomms[0]
}

#[inline(always)]
fn bin_bytes_to_usize(bytes: &[u8]) -> usize {
    bytes
        .iter()
        .rev()
        .enumerate()
        .map(|(pos, &byte)| ((byte - b'0') as usize) << pos)
        .sum::<usize>()
}

#[inline(always)]
fn bit_count_at_bytes(nums: &[&[u8]], bit: usize) -> isize {
    nums
        .iter()
        .map(|&num| {
            if num[bit] == b'1' {
                1isize
            } else {
                -1
            }
        })
        .sum::<isize>()
}

#[aoc(day3, part2, slices)]
pub fn part2_slices(input: &str) -> usize {
    let bin_bytes_to_usize = |bytes: &[u8]| {
        bytes
            .iter()
            .rev()
            .enumerate()
            .map(|(pos, &byte)| ((byte - b'0') as usize) << pos)
            .sum::<usize>()
    };
    let bit_count_at_bytes = |nums: &[&[u8]], bit: usize| {
        nums
            .iter()
            .map(|&num| {
                if num[bit] == b'1' {
                    1isize
                } else {
                    -1
                }
            })
            .sum::<isize>()
    };
    let mut lines = input.lines();
    let first = lines.next().unwrap();
    let bits = first.len();
    let mut nums = input
        .lines()
        .map(|line| line.as_bytes())
        .collect::<Vec<_>>();
    nums.push(first.as_bytes());
    let mut comms = nums.clone();
    let mut uncomms = nums;
    for bit in 0..bits {
        if comms.len() <= 1 && uncomms.len() <= 1 {
            break;
        }
        if comms.len() > 1 {
            let count = bit_count_at_bytes(&comms, bit);
            let comm_bit = if count >= 0 {
                b'1'
            } else {
                b'0'
            };
            let check_bit = |num: &[u8]| num[bit] == comm_bit;
            let mut ind = 0;
            while ind < comms.len() {
                if check_bit(comms[ind]) {
                    ind += 1;
                } else {
                    let _ = comms.swap_remove(ind);
                }
            }
        }
        if uncomms.len() > 1 {
            let count = bit_count_at_bytes(&uncomms, bit);
            let uncomm_bit = if count >= 0 {
                b'0'
            } else {
                b'1'
            };
            let check_bit = |num: &[u8]| num[bit] == uncomm_bit;
            let mut ind = 0;
            while ind < uncomms.len() {
                if check_bit(uncomms[ind]) {
                    ind += 1;
                } else {
                    let _ = uncomms.swap_remove(ind);
                }
            }
        }
    }
    bin_bytes_to_usize(comms[0]) * bin_bytes_to_usize(uncomms[0])
}

#[inline(always)]
fn check_one_at_bytes(num: &[u8], bit: usize) -> bool {
    num[bit] == b'1'
}

#[inline(always)]
fn check_zero_at_bytes(num: &[u8], bit: usize) -> bool {
    num[bit] == b'0'
}

#[aoc(day3, part2, optslices)]
pub fn part2_optslices(input: &str) -> usize {
    let mut lines = input.lines();
    let first = lines.next().unwrap();
    let bits = first.len();
    let mut nums = input
        .lines()
        .map(|line| line.as_bytes())
        .collect::<Vec<_>>();
    nums.push(first.as_bytes());
    let mut comms = nums.clone();
    let mut uncomms = nums;
    for bit in 0..bits {
        if comms.len() <= 1 && uncomms.len() <= 1 {
            break;
        }
        if comms.len() > 1 {
            let count = bit_count_at_bytes(&comms, bit);
            let check_bit = if count >= 0 {
                check_one_at_bytes
            } else {
                check_zero_at_bytes
            };
            let mut ind = 0;
            while ind < comms.len() {
                if check_bit(comms[ind], bit) {
                    ind += 1;
                } else {
                    let _ = comms.swap_remove(ind);
                }
            }
        }
        if uncomms.len() > 1 {
            let count = bit_count_at_bytes(&uncomms, bit);
            let check_bit = if count >= 0 {
                check_zero_at_bytes
            } else {
                check_one_at_bytes
            };
            let mut ind = 0;
            while ind < uncomms.len() {
                if check_bit(uncomms[ind], bit) {
                    ind += 1;
                } else {
                    let _ = uncomms.swap_remove(ind);
                }
            }
        }
    }
    bin_bytes_to_usize(comms[0]) * bin_bytes_to_usize(uncomms[0])
}
